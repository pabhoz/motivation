import { MotivationPage } from './app.po';

describe('motivation App', () => {
  let page: MotivationPage;

  beforeEach(() => {
    page = new MotivationPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
