import { Component, OnInit } from '@angular/core';
import { Estudiante } from "app/estudiante/estudiante";
import { EstudianteDataService } from "app/estudiante/estudiante-data.service";

@Component({
  selector: 'app-chevere',
  templateUrl: './chevere.component.html',
  styleUrls: ['./chevere.component.scss'],
  providers: [EstudianteDataService]
})
export class ChevereComponent implements OnInit {
  
  newEst: Estudiante = new Estudiante("Luisa",8);

  constructor(public estudianteDataService: EstudianteDataService) { }

  ngOnInit() {
  }

  addEstudiante() {
    this.estudianteDataService.addEstudiante(this.newEst);
    this.newEst = new Estudiante(this.newEst.nombre,this.newEst.semestre);
  }

  get all() {
    return this.estudianteDataService.getAll();
  }

  printNombre(){
    console.log(this.newEst.nombre);
  }

}
