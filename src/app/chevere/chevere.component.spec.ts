import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChevereComponent } from './chevere.component';

describe('ChevereComponent', () => {
  let component: ChevereComponent;
  let fixture: ComponentFixture<ChevereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChevereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChevereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
