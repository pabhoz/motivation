import { TestBed, inject } from '@angular/core/testing';

import { EstudianteDataService } from './estudiante-data.service';

describe('EstudianteDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstudianteDataService]
    });
  });

  it('should ...', inject([EstudianteDataService], (service: EstudianteDataService) => {
    expect(service).toBeTruthy();
  }));
});
