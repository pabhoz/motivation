import { Injectable } from '@angular/core';
import { Estudiante } from "app/estudiante/estudiante";

@Injectable()
export class EstudianteDataService {

  lastId: number = 0;
  estudiantes: Estudiante[] = [];

  constructor() {
  }

  addEstudiante(est: Estudiante): EstudianteDataService {
    if (!est.id) {
      est.id = ++this.lastId;
    }
    this.estudiantes.push(est);
    return this;
  }


  getAll(): Estudiante[] {
    return this.estudiantes;
  }

}
